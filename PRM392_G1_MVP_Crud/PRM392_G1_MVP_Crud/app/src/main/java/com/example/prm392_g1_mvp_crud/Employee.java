package com.example.prm392_g1_mvp_crud;

public class Employee {
    public int Id;
    public String fullName;
    public String hireDate;
    public double salary;

    public Employee(int id, String fullName, String hireDate, double salary) {
        Id = id;
        this.fullName = fullName;
        this.hireDate = hireDate;
        this.salary = salary;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHireDate() {
        return hireDate;
    }

    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }


    @Override
    public String toString() {
        return Id+ " - "+fullName+ " - "+hireDate+" - "+salary;
    }
}
