package com.example.prm392_g1_mvp_crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

public  class MainActivity extends AppCompatActivity implements EmployeeContract.View {
    private ListView lv;
    private ArrayAdapter adapter;
    private Button btn_add;
    private EmployeeContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = findViewById(R.id.lv_listemployee);
        btn_add = findViewById(R.id.btn_add);

        presenter = new EmployeePresenter(this, this);
        presenter.onViewCreated();

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAddButtonClicked();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                presenter.onUpdateButtonClicked(position);
            }
        });
    }

    @Override
    public void showUpdateDialog(Employee employee) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.detail_employee, null);
        builder.setView(dialogView);

        EditText editFullName = dialogView.findViewById(R.id.fullname);
        EditText editHireDate = dialogView.findViewById(R.id.date);
        EditText editSalary = dialogView.findViewById(R.id.et_uaddress);

        editFullName.setText(employee.getFullName());
        editHireDate.setText(employee.getHireDate());
        editSalary.setText(String.valueOf(employee.getSalary()));

        builder.setTitle("Update Employee");

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fullName = editFullName.getText().toString();
                String hireDate = editHireDate.getText().toString();
                double salary = Double.parseDouble(editSalary.getText().toString());

                presenter.onSaveUpdateButtonClicked(employee.getId(), fullName, hireDate, salary);
            }
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.onDeleteButtonClicked(employee.getId());
            }
        });

        builder.create().show();
    }

    @Override
    public void showAddDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_employee_layout, null);
        builder.setView(dialogView);

        EditText editFullName = dialogView.findViewById(R.id.et_fullName);
        EditText editHireDate = dialogView.findViewById(R.id.et_hireDate);
        EditText editSalary = dialogView.findViewById(R.id.et_salary);

        builder.setTitle("Add Employee");

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fullName = editFullName.getText().toString();
                String hireDate = editHireDate.getText().toString();
                double salary = Double.parseDouble(editSalary.getText().toString());

                // Call presenter method to add employee
                presenter.onSaveButtonClicked(fullName, hireDate, salary);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }


    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void refreshEmployeeList(List<Employee> employees) {
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, employees);
        lv.setAdapter(adapter);
    }
}




