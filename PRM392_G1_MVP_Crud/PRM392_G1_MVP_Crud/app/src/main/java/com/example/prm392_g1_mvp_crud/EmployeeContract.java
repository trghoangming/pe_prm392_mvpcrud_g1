package com.example.prm392_g1_mvp_crud;


import java.util.List;

public interface EmployeeContract {
        interface View {
            void showAddDialog();
            void showUpdateDialog(Employee employee);
            void showToast(String message);
            void refreshEmployeeList(List<Employee> employees);
        }

        interface Presenter {
            void onAddButtonClicked();
            void onUpdateButtonClicked(int position);
            void onSaveButtonClicked(String fullName, String hireDate, double salary);
            void onDeleteButtonClicked(int employeeId);
            void onViewCreated();
            public void onSaveUpdateButtonClicked(int employeeId, String fullName, String hireDate, double salary) ;
        }
    }

