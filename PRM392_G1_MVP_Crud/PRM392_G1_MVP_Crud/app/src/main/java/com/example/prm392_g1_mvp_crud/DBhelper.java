package com.example.prm392_g1_mvp_crud;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBhelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "MVPEmployee.db";
    public static final int DATABASE_VERSION = 1;

    public DBhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE Employee2 (Id INTEGER PRIMARY KEY AUTOINCREMENT, fullName TEXT, hireDate TEXT, salary DOUBLE)";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Employee2");
        onCreate(db);
    }

    public long insertEmployee(String fullName, String hireDate, double salary) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("fullName", fullName);
        values.put("hireDate", hireDate);
        values.put("salary", salary);
        long result = db.insert("Employee2", null, values);
        db.close();
        return result;
    }

    public List<Employee> getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Employee2", null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex("Id"));
                String fullName = cursor.getString(cursor.getColumnIndex("fullName"));
                String hireDate = cursor.getString(cursor.getColumnIndex("hireDate"));
                double salary = cursor.getDouble(cursor.getColumnIndex("salary"));
                Employee employee = new Employee(id, fullName, hireDate, salary);
                employees.add(employee);
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();
        return employees;
    }

    public boolean deleteEmployee(int employeeId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rowsAffected = db.delete("Employee2", "Id=?", new String[]{String.valueOf(employeeId)});
        db.close();
        return rowsAffected > 0;
    }
    public boolean updateEmployee(int employeeId, String fullName, String hireDate, double salary) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("fullName", fullName);
        values.put("hireDate", hireDate);
        values.put("salary", salary);
        int rowsAffected = db.update("Employee2", values, "Id=?", new String[]{String.valueOf(employeeId)});
        db.close();
        return rowsAffected > 0;
    }
}
