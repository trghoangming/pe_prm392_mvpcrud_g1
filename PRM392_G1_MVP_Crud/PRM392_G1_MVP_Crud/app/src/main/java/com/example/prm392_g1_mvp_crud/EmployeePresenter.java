package com.example.prm392_g1_mvp_crud;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class EmployeePresenter implements EmployeeContract.Presenter {
    private EmployeeContract.View view;
    private DBhelper dbHelper;

    public EmployeePresenter(EmployeeContract.View view, Context context) {
        this.view = view;
        dbHelper = new DBhelper(context);
    }

    @Override
    public void onAddButtonClicked() {
        view.showAddDialog();
    }

    @Override
    public void onUpdateButtonClicked(int position) {
        List<Employee> employees = dbHelper.getAllEmployees();
        if (position >= 0 && position < employees.size()) {
            Employee employee = employees.get(position);
            view.showUpdateDialog(employee);
        }
    }

    @Override
    public void onSaveButtonClicked(String fullName, String hireDate, double salary) {
        long result = dbHelper.insertEmployee(fullName, hireDate, salary);
        if (result != -1) {
            List<Employee> employees = dbHelper.getAllEmployees();
            view.refreshEmployeeList(employees);
            view.showToast("Employee added successfully");
        } else {
            view.showToast("Failed to add employee");
        }
    }

    @Override
    public void onSaveUpdateButtonClicked(int employeeId, String fullName, String hireDate, double salary) {
        if (dbHelper.updateEmployee(employeeId, fullName, hireDate, salary)) {
            // Notify view about successful update
            view.showToast("Employee updated successfully");
            view.refreshEmployeeList(dbHelper.getAllEmployees());
        } else {
            // Notify view about update failure
            view.showToast("Failed to update employee");
        }
    }


    @Override
    public void onDeleteButtonClicked(int employeeId) {
        boolean success = dbHelper.deleteEmployee(employeeId);
        if (success) {
            List<Employee> employees = dbHelper.getAllEmployees();
            view.refreshEmployeeList(employees);
            view.showToast("Employee deleted successfully");
        } else {
            view.showToast("Failed to delete employee");
        }
    }

    @Override
    public void onViewCreated() {
        List<Employee> employees = dbHelper.getAllEmployees();
        view.refreshEmployeeList(employees);
    }
}